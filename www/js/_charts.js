(function() {

Highcharts.chart('zbrojni_prukazy', {
    chart: {
        type: 'column'
    },
    credits: {
        text: 'Zdroj dat: Policejní prezidium ČR, Ředitelství SZBM / LEX',
        href: 'http://gunlex.cz/clanky/hlavni-clanky/2641-pocet-drzitelu-zbrojnich-prukazu-a-zbrani-v-roce-2016'
    },
    colors: ['#bababa', '#377eb8', '#4daf4a', '#f7a35c', '#e41a1c'],
    title: {
        text: 'Počty držitelů zbrojních průkazů'
    },
    xAxis: {
        categories: [
            '2003',
            '2004',
            '2005',
            '2006',
            '2007',
            '2008',
            '2009',
            '2010',
            '2011',
            '2012',
            '2013',
            '2014',
            '2015',
            '2016'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        labels: {
            formatter: function() {
            return this.value / 1000 + ' tis.'
            }   
        },
        title: {
            text: 'počet držitelů'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'sběratelé (A)',
        data: [
                73368,
                73555,
                74466,
                74887,
                74945,
                75829,
                80754,
                81117,
                82229,
                82572,
                83619,
                85463,
                87877,
                94307
        ]

    }, {
        name: 'sportovní střelci (B)',
        data: [
                121922,
                123594,
                124841,
                124244,
                124656,
                126886,
                131504,
                133879,
                134618,
                134546,
                135345,
                137284,
                139967,
                148769
        ]

    }, {
        name: 'lovci (C)',
        data: [
                135149,
                131300,
                128957,
                122451,
                114646,
                112776,
                113281,
                112021,
                107559,
                105274,
                104592,
                105931,
                107599,
                109992
        ]

    }, {
        name: 'profesionálové (D)',
        data: [
                65876,
                62525,
                63961,
                63568,
                63305,
                64918,
                65955,
                65597,
                64840,
                62889,
                62175,
                62286,
                62742,
                63938
        ]

    }, {
        name: 'ochrana zdraví a majetku (E)',
        data: [
                227583,
                228978,
                230784,
                228492,
                229166,
                232862,
                238056,
                236686,
                234362,
                230648,
                229091,
                229579,
                231854,
                241229
        ]

    }]
});

})();

(function () {
Highcharts.chart('tc_d', {
    chart: {
        height: 650,
        type: 'bar'
    },
    title: {
        text: 'Trestné činy se zbraní sk. D'
    },
    xAxis: {
        categories: [
            'Nebezpečné vyhrožování',
            'Loupeže',
            'Výtržnictví',
            'Vydírání',
            'Úmyslné ublížení na zdraví',
            'Loupeže na finančních institut.',
            'Násilí proti policistovi',
            'Vraždy motivované osobními vztahy',
            'Porušování domovní svobody',
            'Vraždy ostatní',
            'Znásilnění',
            'Týrání osoby žijící  ve společném obydlí',
            'Výtržnictví na sportovních a veřejných akcích',
            'Ublížení na zdraví z nedbalosti',
            'Poškozování cizí věci',
            'Krádeže v jiných objektech',
            'Ohrožení pod vlivem návykové látky, opilství',
            'Násilí a vyhrožování proti skupině obyvatel',
            'Násilí proti obecnímu policistovi.',
            'Násilí proti úřední osobě',
            'Podpora a propagace hnutí',
            'Rvačky',
            'Hanobení národa, rasy, etnické nebo jiné skupiny ',
            'Krádeže jiné na osobách',
            'Krádeže vozidel dvoustopých',
            'Ublížení na zdraví - pracovní úraz',
            'Týrání zvířat',
            'Vraždy loupežné '
        ],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Počet trestných činů',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ''
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Rok 2016',
        data: [
                212,
                153,
                73,
                68,
                46,
                26,
                8,
                6,
                6,
                3,
                3,
                3,
                2,
                2,
                2,
                2,
                2,
                2,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1
        ]
    }]
});
})();

(function () {
Highcharts.chart('tc_abc_leg', {
    chart: {
        type: 'bar'
    },
    colors: ['#e41a1c'],
    title: {
        text: 'Trestné činy se zbraní sk. A, B a C'
    },
    xAxis: {
        categories: [
            'Nebezpečné vyhrožování',
            'Vraždy motivované osobními vztahy',
            'Výtržnictví',
            'Vydírání',
            'Vraždy ostatní',
            'Úmyslné ublížení na zdraví',
            'Loupeže',
            'Týrání osoby žijící  ve společném obydlí',
            'Ublížení na zdraví z nedbalosti'
        ],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Počet trestných činů',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ''
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Rok 2016',
        data: [
                17,
                6,
                6,
                3,
                3,
                3,
                3,
                2,
                2
        ]
    }]
});
})();

(function () {
Highcharts.chart('tc_abc_neleg', {
    chart: {
        type: 'bar'
    },
    colors: ['#7a0177'],
    title: {
        text: 'Trestné činy s nelegálně drženou zbraní sk. A, B a C'
    },
    xAxis: {
        categories: [
            'Loupeže',
            'Nebezpečné vyhrožování',
            'Úmyslné ublížení na zdraví',
            'Vraždy motivované osobními vztahy',
            'Vraždy ostatní',
            'Ublížení na zdraví z nedbalosti',
            'Vydírání',
            'Vraždy loupežné',
            'Násilí proti policistovi',
            'Výtržnictví',
            'Pytláctví a ostatní majetkové trestné činy',
            'Násilí a vyhrožování proti skupině obyvatel',
            'Násilí proti úřední osobě',
            'Loupeže na finančních institutcích'
        ],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Počet trestných činů',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ''
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Rok 2016',
        data: [
                24,
                16,
                7,
                5,
                4,
                3,
                3,
                2,
                2,
                1,
                1,
                1,
                1,
                1
        ]
    }]
});
})();