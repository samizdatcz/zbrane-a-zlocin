(function ( $ ) {   
 
    if($ == undefined) {
        console.log("Boxy s odkazy vyzaduji jQuery, pardon");
        return;
    }

    var stylesInited = false;


    $.fn.boxOdkazy = function( options ) {
        
        var settings = $.extend({
            url: "https://interaktivni.rozhlas.cz/proxy/rozhlas/zpravy/rss_hp",
            limit: 3,
            sticky: true,
            stickyTopPadding: 60,
            width: 150,
            leftPadding: 40,
            minViewportWidth: 1340,
            blacklist: []
        }, options );

        // TODO: pokud by melo byt vice ruznych BSO na strance, tak se styly budou muset rozdelit na ty fixni
        // a ty, ktere maji nejakou vazbu na zadane vlastnosti (napr. sirka boxu). Kazdemu boxu bude muset byt 
        // prirazeno unikani cislo a do <style> pridany prislusne pravilda
        var addStyles = function() {
            if(stylesInited) return;

            var sheet = document.createElement('style');
            sheet.innerHTML = '.bso-boxed {\
                                    position: relative;\
                                }\
                                .bso-box {\
                                    width: '+ settings.width +'px;\
                                    position: absolute;\
                                    top: 0;\
                                    right: -'+(settings.width + settings.leftPadding)+'px;\
                                }\
                                .bso-link {\
                                    display: block\
                                }\
                                .bso-logo {\
                                    text-align: left;\
                                    display: block;\
                                    margin: 0;\
                                    padding: 0;\
                                    margin-bottom: 5px\
                                }\
                                .bso-more { \
                                    color: #ed2e38;\
                                    font-family: sans-serif;\
                                    font-size: 12px;\
                                    margin-top: 0px;\
                                    font-weight: bold;\
                                    text-decoration: underline;\
                                }\
                                .bso-title {\
                                    font-family: sans-serif;\
                                    font-size: 12px;\
                                    line-height: 1.4;\
                                    padding-bottom: 0;\
                                    margin: 0;\
                                    margin-top: 5px !important;\
                                }\
                                .bso-news {\
                                    display: block;\
                                    margin-bottom: 15px;\
                                    text-decoration: none;\
                                    color: black;\
                                }\
                                .bso-image {\
                                    width: 100%;\
                                }\
                                @media only screen and (max-width: ' + (settings.minViewportWidth - 1) + 'px) {\
                                    .bso-box {\
                                        display: none;\
                                    }\
                                }';

            document.body.appendChild(sheet);
            stylesInited = true;
        }

        var getData = function(url) {
            return $.ajax({
                  url: url,
                  dataType: "xml"
                });
        } 


        var renderBox = function(element, news) {
            var str = '<div class="bso-box">';
            str += '<a class="bso-logo" href="http://zpravy.rozhlas.cz">' + getLogo() +  "</a>";
            $.each(news, function(i) {
                str += renderNews(this, i + 1);
            });

            str += '<a href="http://zpravy.rozhlas.cz" class="bso-more">Další aktuální zprávy</a>';
            str += '</div>';

            element.append(str);
            var box = element.find(".bso-box");

            box.find(".bso-logo").click(function() {
                if(typeof _gaq != "undefined") {
                    _gaq.push(['_trackEvent', 'zpravy_data_aktualne-logo', 'clicked']);    
                }
                
            });
            box.find(".bso-more").click(function() {
                if(typeof _gaq != "undefined") {
                    _gaq.push(['_trackEvent', 'zpravy_data_aktualne-dalsi', 'clicked']);
                }
            });

            box.find(".bso-news").click(function() {
                if(typeof _gaq != "undefined") {
                    _gaq.push(['_trackEvent', 'zpravy_data_aktualne' + $(this).data("order") , 'clicked']);
                }
            });


            if(settings.sticky) {
                element.find(".bso-box").stick_in_parent({
                        parent:$("body"),
                        offset_top: settings.stickyTopPadding
                    });
            }
        }

        var renderNews = function(news, order) {
            var str = "";
            str += "<a class='bso-news' data-order='" + order + "' href='" + news.link + "'>"
                str += "<img class='bso-image' src='" + news.image + "' />";
                str += "<h3 class='bso-title'>" + news.title + "</h3>";
            str += "</a>";

            return str;
        }

        var getLogo = function() {
            var logo = '<svg width="110" height="30" xmlns="http://www.w3.org/2000/svg">';
            logo += '<g><g stroke="null" id="svg_5"><g stroke="null" id="svg_7"><g stroke="null" id="svg_3" class="logo"><path stroke="null" fill="#ED2E38" d="m8.73203,-0.037l-8.66403,0l0,4.30745l17.48007,0c-1.26282,-2.94291 -4.58891,-4.30745 -8.81604,-4.30745m8.22364,11.0579l-16.88767,0l0,-4.293l18.0193,0c0.00867,0.16819 0.01329,0.3381 0.01156,0.51207c-0.01503,1.59745 -0.39705,2.82906 -1.14319,3.78095m-16.88767,2.45744l0,4.28549l15.7416,0l-2.63545,-4.28549l-13.10616,0l0.00001,0zm0,6.74293l17.25409,0l2.65452,4.31555l-19.90862,0l0,-4.31555l0.00001,0z" id="svg_1"/></g>';
            logo += '<g stroke="null" id="svg_4" class="logotyp"><path stroke="null" fill="#ED2E38" d="m104.4277,11.01339l-2.24417,9.10732l-2.45281,-9.10732l-4.1751,0l3.91387,10.95965c0.36584,0.99176 0.54847,1.38303 0.54847,1.93093c0,0.88715 -0.39185,1.69686 -1.9315,1.69686l-1.27842,0l0,3.02614c0.10461,0 0.60049,0.05259 1.01777,0.05259c2.4008,0 3.94046,-0.0786 4.93164,-1.33102c0.57449,-0.70452 1.04377,-2.19158 1.279,-2.92269l4.46177,-13.41246l-4.0705,0m-9.3674,0l-4.07107,0l-2.24417,8.37622l-2.47883,-8.37622l-4.09651,0l4.67041,13.51708l3.6798,0l4.54037,-13.51708zm-13.69971,6.002c0,-1.09637 0.05201,-2.66203 -0.36584,-3.67923c-0.99118,-2.34879 -3.5226,-2.71405 -5.76678,-2.71405c-3.05273,0 -5.58414,1.30443 -5.76678,4.67098l3.54919,0c-0.05259,-1.33102 1.30443,-1.69628 2.37421,-1.69628c0.80913,0 2.06154,0.20864 2.06154,1.25241c0,0.96576 -1.40904,1.01777 -2.08755,1.12238c-2.92269,0.41728 -6.65393,0.75654 -6.65393,4.59238c0,2.76607 2.08755,4.38316 4.697,4.38316c1.93093,0 2.76607,-0.59991 4.25313,-1.6171l0.02601,1.22641l3.6798,-0.02601l0,-7.51507l0.00001,0.00001zm-3.80984,3.13133c-0.99176,1.72229 -2.87067,1.7483 -3.15734,1.7483c-0.78312,0 -1.80089,-0.33926 -1.80089,-1.46163c0,-1.7483 2.9487,-1.7743 4.01848,-2.06154c0.33926,-0.07802 0.57449,-0.18263 0.93975,-0.31267l0,2.08755l0,-0.00001zm2.49848,-14.63078l-4.30456,0l-1.75177,3.64224l2.79092,0l3.26541,-3.64224zm-15.59827,5.49745l-3.75725,0l0,13.51708l3.93988,0l0,-6.65393c0,-1.61826 0.31325,-3.65321 3.07931,-3.65321c0.33926,0 0.5479,0.02601 0.91316,0.07802l0,-3.4966c-0.33926,-0.05201 -0.49588,-0.05201 -0.67851,-0.05201c-1.64369,0 -2.76548,0.59991 -3.4966,2.47883l0,-2.21817l0.00001,-0.00001zm-9.70666,6.52389c0,1.7483 -0.5479,4.12251 -2.71405,4.12251c-2.24417,0 -2.73948,-1.95694 -2.73948,-3.78325c0,-1.8529 0.41728,-4.07107 2.76548,-4.07107c2.06154,0 2.68804,2.00953 2.68804,3.73182zm-9.26337,-6.52389l0,17.74419l3.83585,0l0,-5.40151c1.04377,1.04377 1.95694,1.51365 3.47058,1.51365c3.91387,0 5.8448,-3.5226 5.8448,-7.0978c0,-4.1491 -2.00895,-7.0978 -5.61015,-7.0978c-1.67027,0 -2.81808,0.6265 -3.80984,1.95752l0,-1.61826l-3.73124,0l0,0.00001zm-17.01366,-4.90564l0,3.44457l9.55061,0l-10.09851,11.87281l0,3.10532l15.57804,0l0,-3.39256l-10.17653,0l9.96789,-11.79421l0,-3.23594l-14.82151,0z" id="svg_2"/></g></g></g></g></svg>';
            return logo;
        }
 
        return this.each(function() {
            var element = $(this);
            getData(settings.url).done(function(data) {
                
                addStyles();

                element.addClass("bso-boxed");

                var zpravy = [];

                var i = 0;
                $(data).find("item").each(function() {
                    var item = $(this);
                    var zprava = {};
                    zprava.title = item.find("title").text();
                    zprava.link = item.find("link").text();
                    if(settings.blacklist.indexOf(zprava.link) == -1) {
                      zprava.image = item.find("enclosure").attr("url");
                      zpravy.push(zprava);
                      i++;
                      if(i == settings.limit) return false;
                    }
                    
                });

                renderBox(element, zpravy);



                
            });
        });
 
    };

    $("[data-bso]").boxOdkazy();
 
}( typeof jQuery == "undefined" ? undefined : jQuery));


// Generated by CoffeeScript 1.9.2

/**
@license Sticky-kit v1.1.2 | WTFPL | Leaf Corcoran 2015 | http://leafo.net
 */

(function() {
  var $, win;

  $ = this.jQuery || window.jQuery;

  win = $(window);

  $.fn.stick_in_parent = function(opts) {
    var doc, elm, enable_bottoming, fn, i, inner_scrolling, len, manual_spacer, offset_top, parent_selector, recalc_every, sticky_class;
    var left;
    if (opts == null) {
      opts = {};
    }
    sticky_class = opts.sticky_class, inner_scrolling = opts.inner_scrolling, recalc_every = opts.recalc_every, parent_selector = opts.parent, offset_top = opts.offset_top, manual_spacer = opts.spacer, enable_bottoming = opts.bottoming;
    if (offset_top == null) {
      offset_top = 0;
    }
    if (parent_selector == null) {
      parent_selector = void 0;
    }
    if (inner_scrolling == null) {
      inner_scrolling = true;
    }
    if (sticky_class == null) {
      sticky_class = "is_stuck";
    }
    doc = $(document);
    if (enable_bottoming == null) {
      enable_bottoming = true;
    }
    fn = function(elm, padding_bottom, parent_top, parent_height, top, height, el_float, detached) {
      var bottomed, detach, fixed, last_pos, last_scroll_height, offset, parent, recalc, recalc_and_tick, recalc_counter, spacer, tick;
      if (elm.data("sticky_kit")) {
        return;
      }
      elm.data("sticky_kit", true);
      last_scroll_height = doc.height();
      parent = elm.parent();
      if (parent_selector != null) {
        parent = parent.closest(parent_selector);
      }
      if (!parent.length) {
        throw "failed to find stick parent";
      }
      fixed = false;
      bottomed = false;
      spacer = manual_spacer != null ? manual_spacer && elm.closest(manual_spacer) : $("<div />");
      if (spacer) {
        spacer.css('position', elm.css('position'));
      }
      recalc = function() {
        var border_top, padding_top, restore;
        if (detached) {
          return;
        }
        last_scroll_height = doc.height();
        border_top = parseInt(parent.css("border-top-width"), 10);
        padding_top = parseInt(parent.css("padding-top"), 10);
        padding_bottom = parseInt(parent.css("padding-bottom"), 10);
        parent_top = parent.offset().top + border_top + padding_top;
        parent_height = parent.height();
        if (fixed) {
          fixed = false;
          bottomed = false;
          if (manual_spacer == null) {
            elm.insertAfter(spacer);
            spacer.detach();
          }
          elm.css({
            position: "",
            top: "",
            width: "",
            bottom: ""
          }).removeClass(sticky_class);
          restore = true;
        }
        top = elm.offset().top - (parseInt(elm.css("margin-top"), 10) || 0) - offset_top;
        left = elm.offset().left;
        height = elm.outerHeight(true);
        el_float = elm.css("float");
        if (spacer) {
          spacer.css({
            width: elm.outerWidth(true),
            height: height,
            display: elm.css("display"),
            "vertical-align": elm.css("vertical-align"),
            "float": el_float
          });
        }
        if (restore) {
          return tick();
        }
      };
      recalc();
      if (height === parent_height) {
        return;
      }
      last_pos = void 0;
      offset = offset_top;
      recalc_counter = recalc_every;
      tick = function() {
        var css, delta, recalced, scroll, will_bottom, win_height;
        if (detached) {
          return;
        }
        recalced = false;
        if (recalc_counter != null) {
          recalc_counter -= 1;
          if (recalc_counter <= 0) {
            recalc_counter = recalc_every;
            recalc();
            recalced = true;
          }
        }
        if (!recalced && doc.height() !== last_scroll_height) {
          recalc();
          recalced = true;
        }
        scroll = win.scrollTop();
        if (last_pos != null) {
          delta = scroll - last_pos;
        }
        last_pos = scroll;
        if (fixed) {
          if (enable_bottoming) {
            will_bottom = scroll + height + offset > parent_height + parent_top;
            if (bottomed && !will_bottom) {
              bottomed = false;
              elm.css({
                position: "fixed",
                bottom: "",
                top: offset,
                left: left
              }).trigger("sticky_kit:unbottom");
            }
          }
          if (scroll < top) {
            fixed = false;
            offset = offset_top;
            if (manual_spacer == null) {
              if (el_float === "left" || el_float === "right") {
                elm.insertAfter(spacer);
              }
              spacer.detach();
            }
            css = {
              position: "",
              width: "",
              left: "",
              top: ""
            };
            elm.css(css).removeClass(sticky_class).trigger("sticky_kit:unstick");
          }
          if (inner_scrolling) {
            win_height = win.height();
            if (height + offset_top > win_height) {
              if (!bottomed) {
                offset -= delta;
                offset = Math.max(win_height - height, offset);
                offset = Math.min(offset_top, offset);
                if (fixed) {
                  elm.css({
                    top: offset + "px"
                  });
                }
              }
            }
          }
        } else {
          if (scroll > top) {
            fixed = true;
            css = {
              position: "fixed",
              top: offset,
              left: left
            };
            css.width = elm.css("box-sizing") === "border-box" ? elm.outerWidth() + "px" : elm.width() + "px";
            elm.css(css).addClass(sticky_class);
            if (manual_spacer == null) {
              elm.after(spacer);
              if (el_float === "left" || el_float === "right") {
                spacer.append(elm);
              }
            }
            elm.trigger("sticky_kit:stick");
          }
        }
        if (fixed && enable_bottoming) {
          if (will_bottom == null) {
            will_bottom = scroll + height + offset > parent_height + parent_top;
          }
          if (!bottomed && will_bottom) {
            bottomed = true;
            if (parent.css("position") === "static") {
              parent.css({
                position: "relative"
              });
            }
            return elm.css({
              position: "absolute",
              bottom: padding_bottom,
              top: "auto"
            }).trigger("sticky_kit:bottom");
          }
        }
      };
      recalc_and_tick = function() {
        recalc();
        return tick();
      };
      detach = function() {
        detached = true;
        win.off("touchmove", tick);
        win.off("scroll", tick);
        win.off("resize", recalc_and_tick);
        $(document.body).off("sticky_kit:recalc", recalc_and_tick);
        elm.off("sticky_kit:detach", detach);
        elm.removeData("sticky_kit");
        elm.css({
          position: "",
          bottom: "",
          top: "",
          width: ""
        });
        parent.position("position", "");
        if (fixed) {
          if (manual_spacer == null) {
            if (el_float === "left" || el_float === "right") {
              elm.insertAfter(spacer);
            }
            spacer.remove();
          }
          return elm.removeClass(sticky_class);
        }
      };
      win.on("touchmove", tick);
      win.on("scroll", tick);
      win.on("resize", recalc_and_tick);
      $(document.body).on("sticky_kit:recalc", recalc_and_tick);
      elm.on("sticky_kit:detach", detach);
      return setTimeout(tick, 0);
    };
    for (i = 0, len = this.length; i < len; i++) {
      elm = this[i];
      fn($(elm));
    }
    return this;
  };

}).call(this);