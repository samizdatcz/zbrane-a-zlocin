---
title: "Zbraně a zločin: české podsvětí si vystačí s pistolemi z tržnice"
perex: "Češi si loni pořídili přes osm tisíc zbrojních průkazů, legální držitelé zbraní se ale až na výjimky do statistik kriminality nedostanou."
description: "Mezi Čechy vloni přibylo přes osm tisíc zbrojních průkazů, legální držitelé zbraní ale až na výjimky do statistik kriminality nedostanou."
authors: ["Jan Cibulka"]
published: "2. března 2017"
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "zbrane-a-zlocin"
libraries: [jquery, highcharts]
styles: ["./styl/charts.css"]
socialimg: https://interaktivni.rozhlas.cz/zbrane-a-zlocin/media/gunny.jpg
recommended:
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/data-vice-strelnych-zbrani-neznamena-automaticky-vice-zastrelenych--1460305
    title: DATA: Více střelných zbraní neznamená automaticky více zastřelených
    perex: Nese s sebou větší množství legálně držených pistolí a pušek větší riziko, že jimi jejich majitelé někoho zabijí? Mezinárodní srovnání ukazuje, že to neplatí zdaleka všude.
    image:  http://media.rozhlas.cz/_obrazek/3233993--krimi-detektivka-lupa-pistole-otisky-prstu-rukavice-akta-zlocin-thriller--1-300x201p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/domaci/_zprava/1664553
    title: PŘEHLEDNĚ: Má váš poslanec zbrojní průkaz a zbraň? Podívejte se na zákonodárce od A po G
    perex: Čeští poslanci a poslankyně mají mnoho různorodých zbraní. Vlastní pistole, pušky i brokovnice.
    image:  http://media.rozhlas.cz/_obrazek/3399623--pistole-munice-naboje-nabojnice-zbrane-nasili-kriminalita--1-300x201p0.jpeg
  - link: https://interaktivni.rozhlas.cz/mpp-poradek/
    title: Za pití v centru můžou večerky, tvrdí pražští strážníci
    perex: Interaktivní mapa míst, kde se v Praze žebrá, pije alkohol či nabízí erotické služby.
    image: https://interaktivni.rozhlas.cz/mpp-poradek/media/a5f73f56d9115b592f50fbeb0c907476/1920x_.jpg
  - link: https://interaktivni.rozhlas.cz/krimi-objasnenost/
    title: Klesá kriminalita? Do policejních statistik se všechny zločiny nedostanou
    perex: Česká policie loni šetřila přes čtvrt milionu případů, je to skoro o 61 tisíc méně než v roce 2011. Státní zástupci ale varují, že je předčasné mluvit o ústupu zločinu.
    image:  https://interaktivni.rozhlas.cz/krimi-objasnenost/media/5038f50f6c9fee3a20d14b9c280b127b/1920x_.jpg
---

<div data-bso="1"></div>

Čeští střelci získali silného zastánce. Ministr vnitra za ČSSD [Milan Chovanec navrhuje](https://apps.odok.cz/veklep-detail?pid=KORNAGNGZSFW), aby právo na zbraň bylo součástí Ústavy. Na návrhu ústavního zákona se v pondělí [neshodla vláda](http://www.rozhlas.cz/zpravy/domaci/_zprava/na-vlade-chovanec-neuspel-o-zmene-pouzivani-zbrani-rozhodne-snemovna--1702695), nyní se jím bude zabývat sněmovna. Český rozhlas se zaměřil na podrobné kriminální statistiky, aby ukázal, jakou roli hrají střelné zbraně v tuzemské zločinnosti.

Policie loni řešila necelé dva tisíce případů, ve kterých měla hrát roli střelná zbraň. U více než poloviny nevíme, zda to byla pistole skutečná, či jen atrapa, protože se tyto zločiny zatím nepodařilo objasnit. Detektivové vyřešili 904 z nich a na nich se noví střelci podle všeho nepodepsali. Pro srovnání, policie za loňský rok eviduje celkem přes 13 tisíc násilných trestných činů.

Mezi Čechy vloni [přibylo přes osm tisíc](http://gunlex.cz/clanky/hlavni-clanky/2641-pocet-drzitelu-zbrojnich-prukazu-a-zbrani-v-roce-2016) zbrojních průkazů. Akceleroval tedy trend, který je možné pozorovat od roku 2014, Češi si pořizují čím dál častěji zbraně ke sportu či osobní obraně. Naopak počty myslivců a osob, které potřebují zbraň k práci, jsou přibližně konstantní.

<aside class="big">
  <div id="zbrojni_prukazy"></div>
</aside>

Pokud český zločinec sáhne po střelné zbrani, obvykle volí ty volně prodejné, které zákon řadí do [kategorie D](http://www.mvcr.cz/clanek/zbrane-podlehajici-zakonu-o-zbranich-a-podminky-jejich-nabyvani-a-drzeni.aspx?q=Y2hudW09Nw%3D%3D). Sem patří například pistole na plastové kuličky (na sport airsoft), znehodnocené, tedy nefunkční zbraně, sebeobranné plynové pistole a všem dobře známé vzduchovky na diabolky. 

<aside class="small">
<h4>Křesadlové pušky a muškety</h4>
Do skupiny D patří i takzvané předovky, tedy zepředu nabíjené zbraně na černý prach. Tyto zbraně jsou, na rozdíl od výše uvedených, teoreticky opravdu nebezpečné, jelikož dokážou vystřelit skutečný projektil, zpravidla olověnou kuli. V praxi je ale jejich použití (zejména při páchání trestní činnosti) dost komplikované, jak ukazuje třeba toto [video](https://www.stream.cz/nicenipovoleno/10005946-kresadlova-puska). 
</aside>

Společných mají několik věcí: až na extrémní případy (například zásah přímo do oka, výstřel poplašné patrony vedle ucha atp.) by neměly být schopné způsobit vážné zranění, koupit si je může volně každý dospělý a často není snadné je rozeznat od zbraní opravdových. Poslední vlastnost pak zločinci využívají, zbraní hrozí a doufají, že se jim oběť podřídí.

<span style="color: #a50f15;">_Článek ukazuje, že při páchání loupežných přepadení (kterých policie evidovala loni půl druhého tisíce) a jiných zločinů hraje "ostrá" střelná zbraň roli jen výjimečně. To ale neznamená, že by na to měl kdokoli tváří v tvář ozbrojenému pachateli sázet. Ke každé zbrani je třeba přistupovat jako ke smrtícímu nástroji a žádný majetek nestojí za poškozené zdraví či ztracený život._</span>

Oblibu volně prodejných zbraní mezi zločinci ilustrují i policejní statistiky, zbraň kategorie D použili pachatelé u více než šesti stovek ze zmíněných 904 zločinů se střelnou zbraní, přičemž většinou sázeli na podobnost mezi zbraní volně prodejnou a tou ostrou: nejčastěji šlo o nebezpečné vyhrožování, loupeže, výtržnictví a vydírání.

<aside class="big">
  <div id="tc_d"></div>
</aside>

<aside class="small">
<h4>Záludná statistika</h4>
Zbraně kategorie D jsou volně prodejné každému, legálně si je tak může pořídit i člověk s trestní minulostí, který má k těm _na zbrojní průkaz_ cestu zavřenou. Policie následně zločiny s nimi vykazuje jako ty spáchané s legálně drženou zbraní (pokud byl pachatel zletilý, což je jediná podmínka pro pořízení zbraně této kategorie), což následně působí zmatky při interpretaci v médiích: tam jsou takové zločiny přičítané držitelům zbrojních průkazů, o které ale ve skutečnosti nejde.
</aside>

Není ale možné říct, že držitelé ostrých zbraní prakticky zločiny nepáchají, jak [tvrdil například ministr vnitra Milan Chovanec](http://demagog.cz/diskusie/405). Vloni jich bylo objasněno 45. Celkem šlo o devět vražd, šest z nich jako vyústění rodinných sporů. Nebezpečného vyhrožování se dopustilo 17 držitelů zbrojního průkazu, tři někoho vydírali a dva svou nedbalostí někomu ublížili.

V čerstvé paměti je i dva roky starý případ masové vraždy v Uherském Brodě, kdy duševně narušený pachatel zastřelil osm náhodně vybraných osob a jednu těžce zranil, přičemž zbraně v té době držel legálně. V kontextu Česka jde o [výjimečný případ](https://cs.wikipedia.org/wiki/Seznam_%C4%8Desk%C3%BDch_masov%C3%BDch_vrah%C5%AF), v podobném rozsahu vraždila pouze [Olga Hepnarová](https://cs.wikipedia.org/wiki/Olga_Hepnarov%C3%A1), která vjela nákladním automobilem do hloučku lidí čekajících na tramvaj.

<aside class="big">
  <div id="tc_abc_leg"></div>
</aside>

Policie za poslední roky eviduje u kriminality držitelů zbrojních průkazů mírně sestupnou tendenci, před čtyřmi lety šlo o 71 případů, v roce 2014 jich bylo 51, vloni 45. To odpovídá i celkovému [poklesu kriminality](https://interaktivni.rozhlas.cz/krimi-objasnenost/), ke kterému dochází v posledních letech.

Příliš časté paradoxně není ani použití nelegálně držené zbraně, vloni došlo k 71 takovým incidentům. Šlo o 9 vražd, u pěti z nich tak skončily domácí spory. Dále se jednalo o 24 loupeží, 16 případů nebezpečného vyhrožování, sedm případů ublížení na zdraví a jednu bankovní loupež.

<aside class="big">
  <div id="tc_abc_neleg"></div>
</aside>

<aside class="small">
<h4>Legální obrana nelegální pistolí</h4>
Policejní statistika případy eviduje podle klasifikace, kterou jim přidělili vyšetřovatelé. Je možné, že soudy následně některé případy překlasifikovaly na jiný trestný čin či pachatele osvobodily: třeba pro nedostatek důkazů nebo proto, že původně trestné jednání [přehodnotily jako nutnou obranu](http://www.rozhlas.cz/zpravy/politika/_zprava/1612638). České právo zná i [legální obranu s nelegálně drženou zbraní](http://nsoud.cz/Judikatura/judikatura_ns.nsf/WebSearch/B78A7182447C0BDDC1257A4E00669FBE?openDocument&Highlight=0).
</aside>

**Dobrý zákon**

Česko má ve srovnání s okolními zeměmi zbraňovou legislativu poměrně vstřícnou, na západ od našich hranic je držení zbraní povolené obvykle jen ke sportovním či loveckým účelům, například v [Německu](http://gunlex.cz/48-zbrane-a-legislativa/legislativa/684-spolkova-republika-nemecko-tezky-zivot-strelcu) si mohou zbraň k osobní obraně pořídit jen vybrané skupiny obyvatel, jako jsou politici či příslušníci exponovaných profesí (soudci, klenotníci).

Přesto ale zbraně nehrají v tuzemsku při trestné činnosti významnější roli, což experti přisuzují místnímu [zákonu o zbraních](https://www.zakonyprolidi.cz/cs/2002-119).

_„Ten zákon je přísný, ale vrcholně demokratický“_, říká k tomu bývalý policista a střelecký instruktor Pavel Černý. Podle něj jsou požadavky na získání zbraně nastavené dostatečně [striktně](https://www.zakonyprolidi.cz/cs/2002-119#p18-1), aby se ke zbraním nedostali lidé násilničtí nebo psychicky nestabilní, následně ale _„každý občan, který splní zákonné podmínky, si může zbraň dle svého uvážení opatřit a nosit ji,“_ dodává. Že český model funguje, dokládá na [průzkumu  Institute for Economics and Peace](http://www.rozhlas.cz/zpravy/svet/_zprava/cesko-je-sestou-nejbezpecnejsi-zemi-sveta-ukazal-pruzkum-nejhur-je-na-tom-syrie--1638594), který Českou republiku hodnotí jako šestou nejbezpečnější zemi světa.

Každý zájemce musí složit teoretickou zkoušku ze znalosti zákona, předvést, že zvládne se zbraní bezpečně zacházet. Musí také projít lékařskou prohlídkou, jejíž částí může být i psychotest, záleží na uvážení lékaře. 

Zákon také vyžaduje čistý trestní rejstřík a osvědčení [spolehlivosti](https://www.zakonyprolidi.cz/cs/2002-119#p23): držitel zbraně nesmí brát drogy, nadměrně pít a chovat se násilnicky, případně být stíhán za násilný trestný čin, což policisté z odboru zbraní a střeliva ověřují v policejních databázích, přestupkovém rejstříku a na obecním úřadě v místě bydliště žadatele.

Pokud držitel zbraně přestane splňovat některou z podmínek, o zbraně okamžitě přijde. Po novele zbraňového zákona, která následovala po hromadné vraždě v Uherském Brodě, mu je mohou policisté prakticky obratem zabavit [přímo u něj doma]( https://www.zakonyprolidi.cz/cs/2002-119#p56-3).

Český přístup ke zbraním už před časem [shrnul Ústavní soud](http://nalus.usoud.cz/Search/GetText.aspx?sz=Pl-16-98). V případě muže s trestní minulostí, který se domáhal zbrojního průkazu, soudci vysvětlili, že  vlastnit a držet zbraň není základní lidské právo. Jde spíše o privilegium pro zákonadbalé, při jehož přiznání je na místě _„postupovat uvážlivě a pozorně“_.
